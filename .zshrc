# Profile section
export PATH='/usr/local/bin:/usr/bin:/home/jamzattack/.scripts'
export EDITOR='emacsclient -nw'
export VISUAL='emacsclient -nw'
export BROWSER=vimb
export RTV_BROWSER=dmenuhandler
export XAUTHORITY=/tmp/Xauthority
export NOTMUCH_CONFIG=$HOME/.config/notmuch-config
export PASSWORD_STORE_DIR=$HOME/.local/share/password-store
export MANPAGER='less -s'

# Have less display colours
export LESS_TERMCAP_mb=$'\e[1;31m'                              # begin bold
export LESS_TERMCAP_md=$'\e[1;33m'                              # begin blink
export LESS_TERMCAP_us=$'\e[01;37m'                             # begin underline
export LESS_TERMCAP_me=$'\e[0m'                                 # reset bold/blink
export LESS_TERMCAP_se=$'\e[0m'                                 # reset reverse video
export LESS_TERMCAP_ue=$'\e[0m'                                 # reset underline

setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt histignorespace                                          # Do not write a command to history if it begins with a space
setopt NO_NOMATCH

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'	# Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"		# Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true				# Automatically find new executables in path
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
HISTFILE=~/.local/share/zsh/zhistory
HISTSIZE=100000
SAVEHIST=100000
WORDCHARS=${WORDCHARS//\/[&.;]}					# Don't consider certain characters part of the word

## Readline stuff
autoload edit-command-line; zle -N edit-command-line;
bindkey '^X^E' edit-command-line
set -o emacs

## Alias section
alias cp='cp -i'                                                # Confirm before overwriting something
alias mv='mv -i'                                                # Confirm before overwriting something
alias df='df -h'                                                # Human-readable sizes
alias gitu='git add . && git commit && git push'                # Git push for the lazy
alias ls='ls --color=auto --group-directories-first'            # Sensible defaults
alias grep='grep --color'                                       # Colour matching strings
alias c22='f22 $(xclip -o -selection clipboard)'                # Stream link from clipboard at 720p
alias termbin='nc termbin.com 9999'                             # Upload output to termbin.com
alias pacman='sudo pacman'
alias wttr='curl wttr.in'
alias vimrc='nvim ~/.config/nvim/init.vim'
alias zhistory='nvim "$HISTFILE"'
alias em='emacsclient -nw'
alias emacsgit="cd ~/.emacs.d; cp config.org README.org; git add elpa *.org && echo 'commit when ready'"

function cheat() { curl cht.sh/"$1" }
function fd() {calc \($(df -m | awk '/mmcblk0/ {print $4}')\
               +$(du -m ~/Downloads | awk 'END {print $1}')\)/1024 }

# Theming section
autoload -U compinit colors
compinit -d
colors

# enable substitution for prompt
setopt prompt_subst

# Arrow prompt
PROMPT='%B%{$fg[blue]%}%(4~|%-1~/.../%2~|%~)%u%{$reset_color%}%B >%{$fg[magenta]%}>%(?.%{$fg[blue]%}.%{$fg[red]%})>%{$reset_color%}%b '

if [[ ! $TERM = "linux" ]]; then
    # Change terminal emulator title
    autoload -Uz add-zsh-hook
    function xterm_title_precmd () {
        print -Pn '\e]2;%~\a'
    }
    function xterm_title_preexec () {
        print -Pn '\e]2;'
        print -n "${(q)1}\a"
    }
    add-zsh-hook -Uz precmd xterm_title_precmd
    add-zsh-hook -Uz preexec xterm_title_preexec
fi

if [[ "$TERM" = "linux" ]]; then
    # base16-woodland colour theme
    echo -en "\e]P0231e18" # Base 00 - Black         
    echo -en "\e]P1d35c5c" # Base 08 - Red           
    echo -en "\e]P2b7ba53" # Base 0B - Green         
    echo -en "\e]P3e0ac16" # Base 0A - Yellow        
    echo -en "\e]P488a4d3" # Base 0D - Blue          
    echo -en "\e]P5bb90e2" # Base 0E - Magenta       
    echo -en "\e]P66eb958" # Base 0C - Cyan          
    echo -en "\e]P7cabcb1" # Base 05 - White         
    echo -en "\e]P89d8b70" # Base 03 - Bright Black  
    echo -en "\e]P9d35c5c" # Base 08 - Bright Red    
    echo -en "\e]PAb7ba53" # Base 0B - Bright Green  
    echo -en "\e]PBe0ac16" # Base 0A - Bright Yellow 
    echo -en "\e]PC88a4d3" # Base 0D - Bright Blue   
    echo -en "\e]PDbb90e2" # Base 0E - Bright Magenta
    echo -en "\e]PE6eb958" # Base 0C - Bright Cyan   
    echo -en "\e]PFe4d4c8" # Base 07 - Bright White  
    clear #for background artifacting
    
    #block cursor
    _block_cursor(){echo -ne "\e[?16;0;128c"}
    precmd_functions+=(_block_cursor)
    
    alias x='startx ~/.config/Xorg/xinitrc'
fi

## Plugins section: Enable fish style features
# Use autosuggestion
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# Use syntax highlighting ## This must be loaded last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
