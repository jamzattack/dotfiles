\version "2.19.82"
\language "english"

\header {
  title = "<++>"
  subtitle = "<++>"
  composer = "<++>"
  arranger = "<++>"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \key <++>
  \time <++>
  \tempo "<++>"
}

music = \relative c' {
  \global
  \clef clef
  % Music goes here
  <++>
}

\score {
  \new Staff \music
  \layout {}
}
