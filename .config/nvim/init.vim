" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

let mapleader = " "

call plug#begin('/home/jamzattack/.config/nvim/plugged')
	Plug 'tpope/vim-commentary'
	Plug 'chrisbra/color_highlight'
	" Plug 'vim-airline/vim-airline'
	" Plug 'vim-airline/vim-airline-themes'
call plug#end()

" Some basics:
	set nocompatible
	filetype plugin on
	syntax on
	set modeline
	set number
	set relativenumber
	set encoding=utf-8
	set notermguicolors t_Co=256
	set splitbelow splitright
	set ignorecase
	set smartcase
	set mouse=a
	colorscheme peachpuff
	let g:airline_theme='angr'
	let g:netrw_banner = 0
	autocmd FileType help wincmd L
	set wrap linebreak textwidth=0

"''''''''''''''''''''''''''''''''''"
"''''DVORAK BLOCK''''''''''''''''''"
"''''''''''''''''''''''''''''''''''"
" " My Dvorak bindings
	noremap h h
	noremap H H
	noremap t j
	noremap T J
	noremap n k
	noremap N K
	noremap s l
	noremap S L
	noremap l n
	noremap L N
	noremap j t
	noremap J T
	noremap k s
	noremap K S

" " Easier navigation on wrapped lines:
	nnoremap t gj
	nnoremap n gk
	nnoremap gt j
	nnoremap gn k

" " Remap C-[h,j,k,l] to <C-w>[h,j,k,l]
	noremap <C-h> <C-w>h
	noremap <C-t> <C-w>j
	noremap <C-n> <C-w>k
	noremap <C-s> <C-w>l

" " Allow vim-surround to be used with htns
	nmap cr cs
	nmap yr ys

"''''''''''''''''''''''''''''''''''"
"''''QWERTY BLOCK''''''''''''''''''"
"''''''''''''''''''''''''''''''''''"
" " Remap C-[h,j,k,l] to <C-w>[h,j,k,l]
	" nmap <C-h> <C-w>h
	" nmap <C-t> <C-w>t
	" nmap <C-n> <C-w>n
	" nmap <C-s> <C-w>s

" Easier navigation on wrapped lines:
	" nnoremap j gj
	" nnoremap k gk
	" nnoremap $ g$
	" nnoremap 0 g0
	" nnoremap gj j
	" nnoremap gk k
	" nnoremap g$ $
	" nnoremap g0 0

"''''''''''''''''''''''''''''''''''"

" Insert mode remapping so I can use C-t in my wm
	inoremap <C-Space> 
" Decent remap for yanking:
	nmap Y y$
	vmap Y "+y
	map "? "+

" Remap semicolon to colon for ease
	map ; :
	nnoremap ;; ;

" Remove trailing whitespace
	nmap <leader>w * :%s/\s\+$//e<CR>

" Make line numbers less obvious:
	hi LineNr ctermfg=darkgrey
	hi CursorLineNr ctermfg=lightgrey
	nmap <leader>m :hi LineNr ctermfg=darkgrey \| hi CursorLineNr ctermfg=lightgrey <CR><CR>
	hi Search cterm=NONE ctermfg=white ctermbg=darkgrey

" Set search highlight colors:
	hi Search cterm=NONE ctermfg=white ctermbg=darkgrey

" Toggle line numbers:
	nmap <leader>n :set invrelativenumber \| set invnumber <CR>

" Fold highlight colours:
	hi Folded ctermfg=white ctermbg=darkgrey

" Toggle search highlighting:
	nmap <leader>s :set hlsearch!<CR>

" Toggle colour highlighting:
	nmap <leader>h :ColorToggle<CR>

" Enable autocompletion:
	set wildmode=longest,list,full

" Disable automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Goyo plugin makes text more readable when writing prose:
	nmap <leader>f :Goyo \| set linebreak<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
	nmap <leader>o :setlocal spell! spelllang=en_nz<CR>

" Automatically reload Xresources and Xdefaults when edited:
	autocmd BufWritePost ~/.Xresources,~/.Xdefaults !xrdb %
	autocmd BufWritePost ~/.Xmodmap !xmodmap %

" Compile document, be it groff/lilypond/markdown/etc.
	map <leader>c :w! \| !compiler <c-r>%<CR>

" Open corresponding .pdf/.html or preview
	map <leader>p :!opout <c-r>%<CR><CR>

" Set up hotkey to remove status bar
	let s:hidden_all = 0
function! ToggleHiddenAll()
	if s:hidden_all  == 0
		let s:hidden_all = 1
		set noshowmode
		set noruler
		set laststatus=0
		set noshowcmd
	else
		let s:hidden_all = 0
		set showmode
		set ruler
		set laststatus=2
		set showcmd
	endif
endfunction
	nnoremap <leader>b :call ToggleHiddenAll()<CR>

" Go to next placeholder and enter insert mode:
	nnoremap <leader><leader> <Esc>/<++><Enter>"_c4l

"Filetype Stuff"

" Setting up templates:
	autocmd BufNewFile *.tex	0r ~/Documents/templates/skeleton.tex
	autocmd BufNewFile *.ly		0r ~/Documents/templates/skeleton.ly
	autocmd BufNewFile *.ms		0r ~/Documents/templates/skeleton.ms

" For Lilypond files:
	autocmd filetype lilypond inoremap ;md _\markup{}<Esc>i
	autocmd filetype lilypond inoremap ;mm -\markup{}<Esc>i
	autocmd filetype lilypond inoremap ;mu ^\markup{}<Esc>i
	autocmd filetype lilypond inoremap ;bd \downbow
	autocmd filetype lilypond inoremap ;bu \upbow
	autocmd filetype lilypond setlocal commentstring=%\ %s
