; vim:ft=dosini
;==========================================================
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
background = #ea1d1d1d
background-alt =
foreground = #ebdbb2
foreground-alt = #a00
primary = #dd606060
secondary = #dd909090
alert = #ffae34

date = #458588
battery = #ffdd44
cpu = #cc241d
pulseaudio = #689d6a
xbacklight = #b16286
mpd = #83a698

linedate = #458588
linebattery = #ffdd44
linecpu = #cc241d
linepulseaudio = #689d6a
linexbacklight = #b16286
linempd = #83a698

[bar/jamzattack]
; monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 14
offset-x = 0%
offset-y = 0%
radius = 0.0
fixed-center = false
enable-ipc = true
bottom = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 0
line-color =

border-size = 0
border-color = #ddf

padding-left = 0
padding-right = 0
module-margin-left = 0
module-margin-right = 0

font-0 = mono:pixelsize=9;2
font-1 = Symbola:pixelsize=11;2
font-2 = Symbols Nerd Font:pixelsize=8;1
font-3 = Unifont:pixelsize=8;1

modules-left = i3 bspwm xwindow
modules-center =
modules-right = mpd xbacklight pulseaudio cpu battery date

tray-position = right
tray-padding = 2
tray-background = ${colors.background}

wm-restack = bspwm
; wm-restack = i3

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = "  %title:0:62:…%  "
label-background = ${colors.primary}
format-padding = 5

[module/bspwm]
type = internal/bspwm
label-focused = %icon%
label-focused-background = ${colors.secondary}
label-focused-foreground = #fde
label-focused-padding = 2

label-occupied = %icon%
label-occupied-padding = 2
label-occupied-background = ${colors.primary}

label-urgent = %icon%
label-urgent-background = ${colors.alert}
label-urgent-foreground = ${colors.foreground-alt}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground}
label-empty-background = ${colors.background}
label-empty-padding = 2

ws-icon-0 = 1;!
ws-icon-1 = 2;@
ws-icon-2 = 3;#
ws-icon-3 = 4;$
ws-icon-4 = 5;%
ws-icon-5 = 6;^
ws-icon-6 = 7;&
ws-icon-7 = 8;*

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false
strip-wsnumbers = true
pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #f0f0f0
label-mode-background = ${colors.primary}

ws-icon-0 = 1;!
ws-icon-1 = 2;@
ws-icon-2 = 3;#
ws-icon-3 = 4;$
ws-icon-4 = 5;%
ws-icon-5 = 6;^
ws-icon-6 = 7;&
ws-icon-7 = 8;*

; focused = Active workspace on focused monitor
label-focused = %icon%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline =
label-focused-padding = ${module/bspwm.label-focused-padding}
label-focused-foreground = #333

; unfocused = Inactive workspace on any monitor
label-unfocused = %icon%
label-unfocused-background = ${module/bspwm.label-occupied-background}
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %icon%
label-visible-background = ${self.label-focused-background}
label-visible-underline =
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = (((%icon%)))
label-urgent-background = #ffae34
label-urgent-padding = ${self.label-focused-padding}

[module/mpd]
type = internal/mpd
format-online = <label-song> <icon-prev> <toggle> <icon-next>
format-online-underline = ${colors.linempd}
format-stopped =
format-paused = <icon-prev> <toggle> <icon-next>

icon-prev = ⏪
icon-stop = ⏹
icon-play = "⯈"
icon-pause = ⏸
icon-next = "⏩ "

label-song = %title%
label-song-maxlen = 48
label-song-ellipsis = false

[module/xbacklight]
type = internal/xbacklight

format = <label>
label = "%percentage%% "
format-prefix = "  "
format-prefix-foreground = ${colors.xbacklight}
format-underline = ${colors.linexbacklight}

[module/cpu]
type = internal/cpu
interval = 2
; format-prefix = " "
format-prefix = "  "
format-prefix-foreground = ${colors.cpu}
format-underline = ${colors.linecpu}
label = "%percentage:1%% "

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #4bffdc
label = %percentage_used%%

[module/date]
type = internal/date
interval = 5

date =
date-alt = " (%d/%m)"

time = "%H:%M "
time-alt = "%H:%M "

format-prefix = " 🕒"
format-prefix-foreground = ${colors.date}
format-underline = ${colors.linedate}

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
label-volume = "%percentage%% "
label-volume-foreground = colors.foreground

format-muted = <label-muted>
label-muted = "00% "
label-muted-foreground = colors.foreground

format-muted-underline = ${colors.linepulseaudio}
format-muted-prefix = " 🔇 "
format-muted-prefix-foreground = ${colors.pulseaudio}

ramp-volume-0 = " 🔈"
ramp-volume-1 = " 🔉"
ramp-volume-2 = " 🔊"
ramp-volume-foreground = ${colors.pulseaudio}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1

format-charging = <animation-charging> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>
format-full = <ramp-capacity> <label-full>
format-full-underline = ${colors.linebattery}
format-charging-underline = ${colors.linebattery}
format-discharging-underline = ${colors.linebattery}

animation-charging-0 = " "
animation-charging-1 = " "
animation-charging-2 = " "
animation-charging-3 = " "
animation-charging-4 = " "
animation-charging-framerate = 1000

ramp-capacity-0 = " "
ramp-capacity-0-foreground = #f53c3c
ramp-capacity-1 = " "
ramp-capacity-1-foreground = ${colors.battery}
ramp-capacity-2 = " "
ramp-capacity-2-foreground = ${colors.battery}
ramp-capacity-3 = " "
ramp-capacity-3-foreground = ${colors.battery}
ramp-capacity-4 = " "
ramp-capacity-4-foreground = ${colors.battery}

label-discharging = "%percentage%% "
label-full = "%percentage%% "
label-charging = "%percentage%% "

[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 0
