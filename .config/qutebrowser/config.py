# Autogenerated config.py
# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# Uncomment this to still load settings configured via autoconfig.yml
# config.load_autoconfig()

# Aliases for commands. The keys of the given dictionary are the
# aliases, while the values are the commands they map to.
# Type: Dict
c.aliases = {'w': 'session-save', 'q': 'quit', 'wq': 'quit --save', 'Set': 'set'}

# Default encoding to use for websites. The encoding must be a string
# describing an encoding such as _utf-8_, _iso-8859-1_, etc.
# Type: String
c.content.default_encoding = 'utf-8'

# List of URLs of lists which contain hosts to block.  The file can be
# in one of the following formats:  - An `/etc/hosts`-like file - One
# host per line - A zip-file of any of the above, with either only one
# file, or a file   named `hosts` (with any extension).  It's also
# possible to add a local file or directory via a `file://` URL. In case
# of a directory, all files in the directory are read as adblock lists.
# The file `~/.config/qutebrowser/blocked-hosts` is always read if it
# exists.
# Type: List of Url
c.content.host_blocking.lists = ['/etc/hosts']

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'file://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'qute://*/*')

# Height (in pixels or as percentage of the window) of the completion.
# Type: PercOrInt
c.completion.height = '40%'

# Where to show the downloaded files.
# Type: VerticalPosition
# Valid values:
#   - top
#   - bottom
c.downloads.position = 'bottom'

# Editor (and arguments) to use for the `open-editor` command. The
# following placeholders are defined: * `{file}`: Filename of the file
# to be edited. * `{line}`: Line in which the caret is found in the
# text. * `{column}`: Column in which the caret is found in the text. *
# `{line0}`: Same as `{line}`, but starting from index 0. * `{column0}`:
# Same as `{column}`, but starting from index 0.
# Type: ShellCommand
c.editor.command = ['st', 'nvim', '{file}']

# CSS border value for hints.
# Type: String
c.hints.border = '1px solid #ffae34'

# Characters used for hint strings.
# Type: UniqueCharString
c.hints.chars = 'aoeuidhtns'

# Open new tabs (middleclick/ctrl+click) in the background.
# Type: Bool
c.tabs.background = True

# Switch between tabs using the mouse wheel.
# Type: Bool
c.tabs.mousewheel_switching = True

# Maximum width (in pixels) of tabs (-1 for no maximum). This setting
# only applies when tabs are horizontal. This setting does not apply to
# pinned tabs, unless `tabs.pinned.shrink` is False. This setting may
# not apply properly if max_width is smaller than the minimum size of
# tab contents, or smaller than tabs.min_width.
# Type: Int
c.tabs.max_width = 342

# Page to open if :open -t/-b/-w is used without URL. Use `about:blank`
# for a blank page.
# Type: FuzzyUrl
c.url.default_page = 'file:///home/jamzattack/.config/qutebrowser/startpage.html'

# Page(s) to open at the start.
# Type: List of FuzzyUrl, or FuzzyUrl
c.url.start_pages = 'file:///home/jamzattack/.config/qutebrowser/startpage.html'

# Default zoom level.
# Type: Perc
c.zoom.default = '90%'

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
# Type: List of QtColor, or QtColor
c.colors.completion.fg = ['#fff', '#ccc', '#aaa']

# Background color of the completion widget for odd rows.
# Type: QssColor
c.colors.completion.odd.bg = '#373737'

# Background color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.bg = '#666'

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.top = '#00000000'

# Bottom border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.bottom = '#00000000'

# Background color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.bg = '#bbb'

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.item.selected.border.top = '#bbb'

# Bottom border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.bottom = '#bbb'

# Color gradient end for download text.
# Type: QtColor
c.colors.downloads.stop.fg = 'white'

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
c.colors.hints.bg = '#ffdd44'

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = '#87afd7'

# Foreground color of the URL in the statusbar on successful load
# (https).
# Type: QssColor
c.colors.statusbar.url.success.https.fg = '#afd787'

# Background color of the tab bar.
# Type: QtColor
c.colors.tabs.bar.bg = '#292929'

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = '#333'

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = '#333'

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = '#666'

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = '#666'

# Default monospace fonts. Whenever "monospace" is used in a font
# setting, it's replaced with the fonts listed here.
# Type: Font
c.fonts.monospace = 'xos4 Terminus'

# Font used in the completion widget.
# Type: Font
c.fonts.completion.entry = '9pt monospace'

# Font used in the completion categories.
# Type: Font
c.fonts.completion.category = 'bold 9pt monospace'

# Font used for the debugging console.
# Type: QtFont
c.fonts.debug_console = '9pt monospace'

# Font used for the downloadbar.
# Type: Font
c.fonts.downloads = '9pt monospace'

# Font used for the hints.
# Type: Font
c.fonts.hints = 'bold 9pt monospace'

# Font used in the keyhint widget.
# Type: Font
c.fonts.keyhint = '9pt monospace'

# Font used for error messages.
# Type: Font
c.fonts.messages.error = '9pt monospace'

# Font used for info messages.
# Type: Font
c.fonts.messages.info = '9pt monospace'

# Font used for warning messages.
# Type: Font
c.fonts.messages.warning = '9pt monospace'

# Font used for prompts.
# Type: Font
c.fonts.prompts = '9pt sans-serif'

# Font used in the statusbar.
# Type: Font
c.fonts.statusbar = '9pt monospace'

# Font used in the tab bar.
# Type: QtFont
c.fonts.tabs = '9pt monospace'

# Font family for standard fonts.
# Type: FontFamily
c.fonts.web.family.standard = 'monospace'

# Font family for fixed fonts.
# Type: FontFamily
c.fonts.web.family.fixed = 'monospace'

# Font family for serif fonts.
# Type: FontFamily
c.fonts.web.family.serif = 'serif'

# Font family for sans-serif fonts.
# Type: FontFamily
c.fonts.web.family.sans_serif = 'sans'

# Default font size (in pixels) for regular text.
# Type: Int
c.fonts.web.size.default = 18

# This setting can be used to map keys to other keys. When the key used
# as dictionary-key is pressed, the binding for the key used as
# dictionary-value is invoked instead. This is useful for global
# remappings of keys, for example to map Ctrl-[ to Escape. Note that
# when a key is bound (via `bindings.default` or `bindings.commands`),
# the mapping is ignored.
# Type: Dict
c.bindings.key_mappings = {'<Ctrl+6>': '<Ctrl+^>', '<Ctrl+Enter>': '<Ctrl+Return>', '<Ctrl+j>': '<Return>', '<Ctrl+m>': '<Return>', '<Ctrl+[>': '<Escape>', '<Enter>': '<Return>', '<Shift+Enter>': '<Return>', '<Shift+Return>': '<Return>'}

# Bindings for normal mode
config.bind('D', 'tab-close')
config.bind('H', 'back')
config.bind('L', 'search-prev')
config.bind('N', 'tab-prev')
config.bind('S', 'forward')
config.bind('T', 'tab-next')
config.bind('d', None)
config.bind('dc', 'tab-clone')
config.bind('dd', 'tab-close')
config.bind('dh', 'tab-move - ')
config.bind('dn', 'tab-move - ')
config.bind('ds', 'tab-move + ')
config.bind('dt', 'tab-move + ')
config.bind('h', 'scroll left')
config.bind('l', 'search-next')
config.bind('n', 'scroll up')
config.bind('s', 'scroll right')
config.bind('t', 'scroll down')
