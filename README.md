# Jamie's dotfiles
Disclaimer: These configs may be optimised for 1366x768 resolution.
All of this stuff is designed for my Acer c730 running Manjaro, but should be suitable for most setups.

## Scripts:
Some of these were stolen from [Luke Smith](https://github.com/lukesmithxyz).
+ compiler: Simple compiler script, with support for pdflatex, markdown, lilypond, python, c, and others.
+ dmenuhandler: Kind of messy script which reads a url and pipes it into various programs.
+ dmenumount: Mount external drives via dmenu.
+ dmenupass: This is for use sudo -A simple password input.
+ dmenuunicode: Select emoji from dmenu, pipes it into clipboard and primary selection.
+ extract: Extract various types of files, including .rar, .zip, .tar.*, .xz, etc.
+ lockscreen: Uses maim to take a screenshot, imagemagick apply blur and swirl, and i3lock to lock the screen.
+ opout: Open the corresponding .pdf, .html, or .sent of a particular file.
+ pdf-opener: Lists all .pdf and .ps files in dmenu, and opens them in zathura.
+ qrclip & qrprimary: Uses qrencode to create a qr code and open it in feh.
+ rofiunicode: Same as dmenuunicode, but uses rofi for colour unicode support.
+ scratchmusictoggle & scratchtermtoggle: For use with BSPWM.  Emulates i3's scratchpad feature.

## Notes:
+ .Xmodmap: Replaces Caps (or Mod4) with Escape, and has extensive diacritic support with AltGr.
+ .emoji: Used by dmenuunicode and rofiunicode for a list of emoji.
+ .zshrc: Right prompt shows git status, left prompt shows stderr and working directory.
+ .config/nvim: vim-plug used as plugin manager.  A few useful plugins used in init.vim.  A whole bunch of sensible settings.  Templates included in ~/Documents/templates.
+ .config/sxhkd: Alt (Mod1) used as modifer, can be changed with a simple find and replace. sxhkdrc compatible with and wm. bspc used for bspwm. able to paused with alt+F4.
+ .config/fontconfig: Terminus used as mono font, Source Sans and Serif Pro used as sans and serif.  75-Emojione.conf replaces mono unicode font with Symbola (copy into /etc/fonts/conf.d).
+ .config/polybar: Transparent grey background, coloured info on the right-hand side, workspaces on the left.  Background colour matches my [terminal](https://gitlab.com/jamzattack/st).
+ .config/compton.conf: Inactive dim enabled so I don't need borders in my wm.

## Screenshot:
![](screenshot.png?raw=true)
